﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using SistemaCapas.Modelo;

namespace SistemaCapas.Conexion
{
    public class Conexion
    {
        /// <summary>
        ///Variables globales
        /// </summary>
        private string database;
        private string userId;
        private string password;
        private string server;

        private MySqlConnection conexion;
        private MySqlCommand sentencia;
        private MySqlDataReader lector;
        private MySqlTransaction transaccion;
        private MySqlConnectionStringBuilder cadenaConexion;
        private MySqlDataAdapter tableAdapter;

        private DataTable tabla;

        /// <summary>
        /// Propiedades
        /// </summary>
        public string Database { get => database; }
        public string UserId { get => userId; }
        public string Password { get => password; }
        public string Server { get => server; }

        public Conexion()
        {
            database = "";
            userId = "";
            password = "";
            server = "localhost";
        }

        private MySqlConnection Conectar()
        {
            cadenaConexion = new MySqlConnectionStringBuilder()
            {
                Database = database,
                UserID = userId,
                Password = password,
                Server = server
            };
            return new MySqlConnection(cadenaConexion.ToString());
        }

        /// <summary>
        /// Ejecutar Consulta select
        /// </summary>
        /// <param name="cliente"> Cliente a consultar </param>
        /// <returns> la tabla con los datos del cliente/clientes </returns>
        public DataTable EjecutarConsulta(Cliente cliente)
        {
            using (conexion = Conectar())
            {
                tabla = new DataTable();
                conexion.Open();
                sentencia = conexion.CreateCommand();
                sentencia.CommandText = "SELECT * FROM Clientes";
                tableAdapter = new MySqlDataAdapter(sentencia.CommandText,conexion);
                tableAdapter.Fill(tabla);
                conexion.Close();
            }
            return tabla;
        }
    }
}
