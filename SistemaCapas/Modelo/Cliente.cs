﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaCapas.Modelo
{
    public class Cliente
    {
        public int IdCliente
        {
            get;
            set;
        }
        public string Nombre
        {
            get;
            set;
        }
        public string ApellidoPaterno
        {
            get;
            set;
        }
        public string ApellidoMaterno
        {
            get;
            set;
        }
        public string Telefono
        {
            get;
            set;
        }
        public string Calle
        {
            get;
            set;
        }
        public int NoInterior
        {
            get;
            set;
        }
        public int NoExterior
        {
            get;
            set;
        }

    }
}
